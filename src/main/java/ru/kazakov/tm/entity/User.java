package ru.kazakov.tm.entity;

import ru.kazakov.tm.enumerated.Role;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class User {

    private Long id = System.nanoTime();

    private String login = "";

    private String password = "";

    private String passwordhash = "";

    private Role role;

    private String firstName = "";

    private String middleName = "";

    private String surName = "";

    public User(String login, String firstName, String middleName, String surName, String password, Role role) {
        this.login = login;
        this.firstName = firstName;
        this.middleName = middleName;
        this.surName = surName;
        this.password = password;
        this.passwordhash = hashPassword(password);
        this.role = role;
    }

    public User(String login, String firstName, String middleName, String surName, String password) {
        this.login = login;
        this.firstName = firstName;
        this.middleName = middleName;
        this.surName = surName;
        this.password = password;
        this.passwordhash = hashPassword(password);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
        this.passwordhash = hashPassword(password);
    }

    public String getPasswordhash() {
        return passwordhash;
    }

    public void setPasswordhash(String passwordhash) {
        this.passwordhash = passwordhash;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getSurName() {
        return surName;
    }

    public void setSurName(String surName) {
        this.surName = surName;
    }

    private String hashPassword(final String password) {
        MessageDigest messageDigest = null;
        byte[] digest = new byte[0];
        try {
            messageDigest = MessageDigest.getInstance("MD5");
            messageDigest.reset();
            messageDigest.update(password.getBytes());
            digest = messageDigest.digest();
        } catch (NoSuchAlgorithmException exception) {
            exception.printStackTrace();
        }
        BigInteger bigInt = new BigInteger(1, digest);
        String md5Hex = bigInt.toString(16);
        while (md5Hex.length() < 32) {
            md5Hex = "0" + md5Hex;
        }
        return md5Hex;

    }

}
