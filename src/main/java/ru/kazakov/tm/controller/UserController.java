package ru.kazakov.tm.controller;

import ru.kazakov.tm.entity.User;
import ru.kazakov.tm.service.UserService;

import java.util.List;

public class UserController extends AbstractController {

    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    public int createUser() {
        System.out.println("[CREATE USER]");
        System.out.println("PLEASE, ENTER LOGIN:");
        final String login = scanner.nextLine();
        System.out.println("PLEASE, ENTER FIRSTNAME:");
        final String firstName = scanner.nextLine();
        System.out.println("PLEASE, ENTER MIDDLENAME:");
        final String middleName = scanner.nextLine();
        System.out.println("PLEASE, ENTER SURNAME:");
        final String surName = scanner.nextLine();
        System.out.println("PLEASE, ENTER PASSWORD:");
        String password = scanner.nextLine();
        System.out.println("PLEASE, ENTER ROLE:");
        String role = scanner.nextLine();
        userService.create(login, firstName, middleName, surName, password, role);
        System.out.println("[OK]");
        return 0;
    }

    public int updateUserByIndex() {
        System.out.println("[UPDATING A USER BY INDEX]");
        System.out.println("PLEASE ENTER, USER INDEX:");
        final int index = Integer.parseInt(scanner.nextLine()) - 1;
        final User user = userService.findByIndex(index);
        if (user == null) {
            System.out.println("[FAIL]");
            return 0;
        }
        System.out.println("PLEASE, ENTER LOGIN:");
        final String login = scanner.nextLine();
        System.out.println("PLEASE, ENTER FIRST NAME:");
        final String firstName = scanner.nextLine();
        System.out.println("PLEASE, ENTER MIDDLE NAME:");
        final String middleName = scanner.nextLine();
        System.out.println("PLEASE, ENTER SURNAME:");
        final String surName = scanner.nextLine();
        System.out.println("PLEASE, ENTER PASSWORD:");
        final String password = scanner.nextLine();
        System.out.println("PLEASE, ENTER ROLE:");
        final String role = scanner.nextLine();
        userService.update(user.getId(), login, firstName, middleName, surName, password, role);
        System.out.println("[OK]");
        return 0;
    }

    public int removeUserById() {
        System.out.println("[REMOVE A USER BY ID]");
        System.out.println("PLEASE, ENTER USER ID:");
        final long id = scanner.nextLong();
        final User user = userService.removeById(id);
        if (user == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
        return 0;
    }

    public int removeTaskByIndex() {
        System.out.println("[REMOVE A USER BY ID]");
        System.out.println("PLEASE, ENTER USER INDEX:");
        final int index = scanner.nextInt() - 1;
        final User user = userService.removeByIndex(index);
        if (user == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
        return 0;
    }

    public int removeUserByLogin() {
        System.out.println("[REMOVE USER BY LOGIN]");
        System.out.println("PLEASE, ENTER LOGIN:");
        final String login = scanner.nextLine();
        final User user = userService.removeByLogin(login);
        if (user == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
        return 0;
    }

    public int clearUser() {
        System.out.println("[USERS CLEAR]");
        userService.clear();
        System.out.println("[OK]");
        return 0;
    }

    public void viewUser(final User user) {
        if (user == null) return;
        System.out.println("[VIEW USER]");
        System.out.println("ID: " + user.getId());
        System.out.println("LOGIN: " + user.getLogin());
        System.out.println("FIRST NAME: " + user.getFirstName());
        System.out.println("MIDDLE NAME: " + user.getMiddleName());
        System.out.println("SURNAME: " + user.getSurName());
        System.out.println("PASSWORD: " + user.getPassword());
        System.out.println("PASSWORDHASH: " + user.getPasswordhash());
        System.out.println("ROLE: " + user.getRole());
        System.out.println("[OK]");
    }

    public int viewUserByIndex() {
        System.out.println("ENTER, USER INDEX:");
        final int index = scanner.nextInt() - 1;
        final User user = userService.findByIndex(index);
        viewUser(user);
        return 0;
    }

    public int listUser() {
        System.out.println("[USERS LIST]");
        int index = 1;
        viewUsers(userService.findAll());
        System.out.println("[OK]");
        return 0;
    }

    public void viewUsers(List<User> users) {
        if (users == null || users.isEmpty()) return;
        int index = 1;
        for (final User user : users) {
            System.out.println(index + ". " + user.getId() + ": " + user.getFirstName());
            index++;
        }
    }

}
