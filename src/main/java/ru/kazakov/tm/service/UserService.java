package ru.kazakov.tm.service;

import ru.kazakov.tm.enumerated.Role;
import ru.kazakov.tm.entity.User;
import ru.kazakov.tm.repository.UserRepository;

import java.util.List;

public class UserService {

    private final UserRepository userRepository;

    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public User create(final String login, final String firstName, final String middleName, final String surName, final String password, final String roleString) {
        if (login == null || login.isEmpty()) return null;
        if (firstName == null || firstName.isEmpty()) return null;
        if (middleName == null || middleName.isEmpty()) return null;
        if (surName == null || surName.isEmpty()) return null;
        if (password == null || password.isEmpty()) return null;
        if (roleString == null || roleString.isEmpty()) return null;
        Role role = getRoleFromString(roleString);
        if (role == null) return null;
        return userRepository.create(login, firstName, middleName, surName, password, role);
    }

    public User create(final String login, final String firstName, final String middleName, final String surName, final String password) {
        if (login == null || login.isEmpty()) return null;
        if (firstName == null || firstName.isEmpty()) return null;
        if (middleName == null || middleName.isEmpty()) return null;
        if (surName == null || surName.isEmpty()) return null;
        if (password == null || password.isEmpty()) return null;
        return userRepository.create(login, firstName, middleName, surName, password);
    }

    public void clear() {
        userRepository.clear();
    }

    public List<User> findAll() {
        return userRepository.findAll();
    }

    public User update(final Long id, final String login, final String firstName, final String middleName, final String surName, final String password, final String roleString) {
        if (id == null) return null;
        if (login == null || login.isEmpty()) return null;
        if (firstName == null || firstName.isEmpty()) return null;
        if (middleName == null || middleName.isEmpty()) return null;
        if (surName == null || surName.isEmpty()) return null;
        if (password == null || password.isEmpty()) return null;
        if (roleString == null || roleString.isEmpty()) return null;
        Role role = getRoleFromString(roleString);
        return userRepository.update(id, login, firstName, middleName, surName, password, role);
    }

    public User findById(Long id) {
        if (id == null) return null;
        return userRepository.findById(id);
    }

    public User findByIndex(int index) {
        if (index < 0 || index > userRepository.findAll().size() - 1) return null;
        return userRepository.findByIndex(index);
    }

    private Role getRoleFromString(final String roleString) {
        return Role.valueOf(roleString);
    }

    public User removeById(Long id) {
        if (id == null) return null;
        return userRepository.removeById(id);
    }

    public User removeByIndex(int index) {
        if (index < 0 || index > userRepository.findAll().size() - 1) return null;
        return userRepository.removeByIndex(index);
    }

    public User removeByLogin(String login) {
        if (login == null || login.isEmpty()) return null;
        return userRepository.removeByLogin(login);
    }

}
